"""
It just prints timestamps!
"""
import time
import sys


def main(interval=1):
    while True:
        print(time.time())
        sys.stdout.flush()
        time.sleep(interval)


if __name__ == "__main__":
    if len(sys.argv) > 1:
        main(float(sys.argv[1]))
    else:
        main(interval=1)