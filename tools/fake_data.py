"""
Create fake data for testing purposes.
"""
import json
import math
import sys
import time



def main(interval=1):
    count = 0
    while True:
        dct = {
            'sensor_1': math.cos(math.pi*count/30),
            'sensor_2': math.sin(math.pi*count/30),
            'timestamp': time.time(),
            'count': count
        }
        print(json.dumps(dct))
        sys.stdout.flush()

        count += 1
        time.sleep(interval)


if __name__ == "__main__":
    main(interval=1)