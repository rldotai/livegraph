# Installing NodeJS for Ubuntu 14.04

Here is a quick guide for setting up NodeJS on Ubuntu 14.04.

1. Download the correct gzip'd binaries from `http://nodejs.org`
2. Execute the following command

```sh
# Magically extracts npm and node to `/usr/local/bin`
sudo tar -C /usr/local --strip-components 1 -xzf /path/to/node.tar.gz
```

3. Verify that it is present and working:

```sh
ls -l /usr/local/bin/node
ls -l /usr/local/bin/npm

# Check that it's executable
node -v
```

## Modify `node_modules` location

You may want to do this to avoid having to type `sudo` before installing global modules.

Create (or open) `~/.npmrc` file, add (or alter) the following line

```
prefix="~/.node_modules"
```

Check that it is showing up:

```bash
npm config get prefix
# should be /home/<username>/.node_modules
```

Now, modify your `~/.bashrc` file so that binaries installed in the `node_modules/bin` folder become available:

```
# Added for node binaries
export PATH="$HOME/.node_modules/bin:$PATH"
```

And then refresh your terminal's settings:

```bash
source ~/.bashrc
```

# ECMAScript 6 Javascript Project Skeleton

[I have a project skeleton available](https://gitlab.com/rldotai/cookiecutter-es6)

With the `cookiecutter` utility (available via `pip install cookiecutter`) you can set up a project skeleton via

```bash
cookiecutter https://gitlab.com/rldotai/cookiecutter-es6.git
```

# References

http://www.sitepoint.com/beginners-guide-node-package-manager/ 