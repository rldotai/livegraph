
/**
 * Base class for charts
 */
export default class BaseChart {
    constructor(canvas, options) {
        this.cvs = canvas;
        this.ctx = canvas.getContext('2d');

        // Configuration
        options = options || {};
    }

    /** Return the DataURL of the canvas the chart draws on. */
    toDataURL(){
        return this.cvs.toDataURL();
    }

    /** Set the style of the canvas element the chart draws on.
     * @param {Object}
     */
    setCanvasStyle(props) {
        for (let k in props) {
            this.cvs.style[k] = props[k];
        }
    }

    /** Update the array with new data
     * @param  {Array}
     * @return {null}
     */
    update(newData) {
        // Add new data to the array
        for (let i of newData) {
            this.data.push(i);
        }

        // Remove old data
        this.truncate();

        // Redraw
        this.draw();
    }
}