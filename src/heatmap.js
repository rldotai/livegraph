import Color from "color";
import _ from 'lodash';
import BaseChart from './base';
import debug from './util';


/**
 * A class implementing a "heatmap" style chart.
 */
export default class Heatmap extends BaseChart {
    // Should options be separated in some fashion?
    static defaultOptions = {
        background: "rgb(255,255,255)",
        fontSize: 10,
        fontFamily: 'monospace',
        maxRows: 10,
        maxCols: 20,
        ymax: undefined,
        ymin: undefined,
    };

    constructor(canvas, options) {
        super(canvas, options);

        // Configuration
        this.options = _.merge({}, options, Heatmap.defaultOptions);

        // Set canvas style according to the options
        this.setCanvasStyle({
            background: this.options.background,
        });

        // Set up data structures
        this.data = [];
    }

    draw() {
        // Get information about the chart for drawing purposes.
        let h = this.cvs.clientHeight; // clientWidth vs. just width?
        let w = this.cvs.clientWidth;
        // Number of sub-arrays and their largest size determine chart geometry
        // Alternatively, could specify this beforehand
        let ny = this.data.length;
        let nx = _.max(_.map(this.data, x => x.length));
        let dx = w/nx;
        let dy = h/ny;

        // Perform the actual drawing
        let ctx = this.ctx;
        ctx.save();
        ctx.clearRect(0, 0, w, h);
        for (let y in this.data) {
            // // Value range determines color/scaling
            let ymax = this.options.ymax || _.max(this.data[y]);
            let ymin = this.options.ymin || _.min(this.data[y]);
            for (let x in this.data[y]) {
                let val = 255*(this.data[y][x] - ymin)/(ymax - ymin);
                console.log(x*dx, y*dy, val);
                ctx.fillStyle = "rgb(" + _.round(val) + ',0,0)';
                ctx.fillRect(x*dx, y*dy, dx, dy);
            }
        }
        // Drawing here
        ctx.restore();
    }

    drawLabels() {
        let ctx = this.ctx;

        ctx.save();
        // Drawing here
        ctx.restore();
    }

    /** Remove `n` data points from the chart. If `n` is undefined, then remove
     * points so that there are at most `maxPoints` remaining.
     * @param  {int}
     */
    truncate(n, m) {
        // assert(n >= 0);
        if (n === undefined) {
            if (this.data.length > this.options.maxPoints) {
               this.data = this.data.slice(this.data.length - this.options.maxPoints);
           }
        } else {
            n = _.min(n, this.data.length);
            this.data = this.data.slice(n);
        }
    }
}