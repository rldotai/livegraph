import Color from "color";
import _ from 'lodash';
import BaseChart from './base'
import debug from './util';

/**
 * Class implementing a "sparkline` style chart.
 */
export default class Sparkline extends BaseChart {
    // Should options be separated in some fashion?
    static defaultOptions = {
        background: "rgb(0,0,0)",
        fontSize: 10,
        fontFamily: 'monospace',
        maxPoints: 20,
        ymax: undefined,
        ymin: undefined,
    };

    constructor(canvas, options) {
        super(canvas, options);

        // Configuration
        this.options = _.merge({}, options, Sparkline.defaultOptions);

        // Set canvas style according to the options
        this.setCanvasStyle({
            background: this.options.background,
        });

        // Set up data structures
        this.data = [];

    }

    /** Draw the canvas from the data
     * @return {null}
     */
    draw() {
        // Determine information about canvas, scaling, etc.
        let h = this.cvs.height;
        let w = this.cvs.clientWidth;
        let ymax = this.options.ymax || _.max(this.data);
        let ymin = this.options.ymin || _.min(this.data);
        let xscale = w/this.options.maxPoints;
        let yscale = h/(ymax - ymin);

        // Determine coordinates to draw points
        let pts = _.zip(_.map(_.range(this.data.length), x => x*xscale), 
                        _.map(this.data, y=>h - yscale*(y-ymin)));
        // Clear the canvas
        let ctx = this.ctx;
        ctx.clearRect(0, 0, w, h);
        
        // Formatting
        ctx.save();
        ctx.strokeStyle = "#ffffff";
        ctx.strokeWidth = 2;

        // Start drawing
        ctx.beginPath();
        ctx.moveTo(...pts[0]);
        for(let p of pts) {
            ctx.lineTo(...p);
        }
        ctx.stroke();
        ctx.restore();

        // Draw labels
        this.drawLabels();
    }

    drawLabels() {
        // Compute information for drawing labels
        let h = this.cvs.clientHeight;
        let ymaxString = _.round(_.max(this.data), 2);
        let yminString = _.round(_.min(this.data), 2);

        // Draw the labels
        let ctx = this.ctx;
        ctx.save();
        // Label font should be complementary color
        ctx.fillStyle = Color(this.options.background).negate().rgbString();
        ctx.font = this.options.fontSize + 'px ' + this.options.fontFamily;

        ctx.fillText(_.toString(ymaxString), 0, this.options.fontSize);
        ctx.fillText(_.toString(yminString), 0, h - 4);
        ctx.restore();
    }

    /** Remove `n` data points from the chart. If `n` is undefined, then remove
     * points so that there are at most `maxPoints` remaining.
     * @param  {int}
     */
    truncate(n) {
        if (n === undefined) {
            if (this.data.length > this.options.maxPoints) {
               this.data = this.data.slice(this.data.length - this.options.maxPoints);
           }
        } else {
            n = _.min(n, this.data.length);
            this.data = this.data.slice(n);
        }
    }
}