// import 'babel-polyfill';
require('raw!../html/index.html');

import Chart from 'chart.js';
import ReconnectingWebSocket from 'ReconnectingWebSocket';

window.onload = function() {
    var container = document.getElementById('canvas-container');
    var cvs1 = document.createElement('canvas');
    container.appendChild(cvs1);
    var chart1 = new Chart(cvs1, {
        type: 'line',
        data: [1,1],
    });
}
function handler(event) {
    data = JSON.parse(event.data);
    console.log(data);
    
}


// var ws = new WebSocket('ws://localhost:31337');
var ws = new ReconnectingWebSocket('ws://localhost:31337');
ws.onmessage = handler
